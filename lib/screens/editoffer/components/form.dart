import 'package:Offertini/components/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';

class FormNewOffer extends StatefulWidget {
  const FormNewOffer({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FormNewOfferState();
}

// Adapted from the text form demo in official gallery app:
// https://github.com/flutter/flutter/blob/master/examples/flutter_gallery/lib/demo/material/text_form_field_demo.dart
class _FormNewOfferState extends State<FormNewOffer> {
  String _dropdownValue,
      _articleType,
      _prize,
      _marketName,
      _marketAddress,
      _date;
  double _sliderVal = 50;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: 24.0),
          /*"Article menu" form.*/
          DropdownButton<String>(
            isExpanded: true,
            value: _dropdownValue,
            hint: Row(
              children: [
                Container(
                  child: Icon(
                    Icons.article_outlined,
                    color: Colors.grey[600],
                  ),
                ),
                SizedBox(
                  width: kDefaultPadding + 5,
                ),
                Container(
                  child: Text("Seleccione un artículo *"),
                )
              ],
            ),
            items: [
              DropdownMenuItem<String>(
                value: '1',
                child: Text('Pan'),
              ),
              DropdownMenuItem<String>(
                value: '2',
                child: Text('Carne'),
              ),
              DropdownMenuItem<String>(
                value: '3',
                child: Text('Bebestible'),
              ),
            ],
            onChanged: (value) {
              setState(() {
                _dropdownValue = value;
              });
            },
          ),
          const SizedBox(height: 24.0),
          /*"Article type" form.*/
          TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              filled: true,
              icon: Icon(Icons.menu_open),
              hintText: 'Ej: Marraqueta, Lomo liso, etc',
              labelText: 'Tipo de artículo *',
            ),
            onSaved: (String value) {
              this._articleType = value;
              print('articleType=$_articleType');
            },
          ),
          const SizedBox(height: 24.0),
          /*"Prize" form.*/
          TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              filled: true,
              icon: Icon(Icons.attach_money),
              hintText: 'Ingrese el precio del producto',
              labelText: 'Precio *',
            ),
            keyboardType: TextInputType.number,
            onSaved: (String value) {
              this._prize = value;
              print('prize=$_prize');
            },
          ),
          const SizedBox(height: 24.0),
          /*"Market name" form.*/
          TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              filled: true,
              icon: Icon(Icons.store),
              hintText: '',
              labelText: 'Nombre del negocio *',
            ),
            onSaved: (String value) {
              this._marketName = value;
              print('marketName=$_marketName');
            },
          ),
          const SizedBox(height: 24.0),
          /*"Market address" form.*/
          TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              filled: true,
              icon: Icon(Icons.location_on),
              hintText: 'Ingrese la dirección exacta',
              labelText: 'Dirección del negocio *',
            ),
            onSaved: (String value) {
              this._marketAddress = value;
              print('marketAddress=$_marketAddress');
            },
          ),
          const SizedBox(height: 24.0),
          Container(
            padding: EdgeInsets.only(top: 20, left: 10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[600]),
                borderRadius: BorderRadius.circular(5)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  'Concurrencia actual del negocio *',
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black87,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                const SizedBox(height: 30),
                Slider(
                    value: _sliderVal,
                    min: 0,
                    max: 100,
                    divisions: 2,
                    activeColor: kPrimaryColor,
                    inactiveColor: kPrimaryLightColor,
                    label: _sliderVal.round() == 0.0
                        ? 'Baja'
                        : _sliderVal.round() == 50.0
                            ? 'Media'
                            : 'Alta',
                    onChanged: (double value) {
                      setState(() => _sliderVal = value);
                    })
              ],
            ),
          ),
          const SizedBox(height: 24.0),
          Container(
            width: size.width * 0.8,
            padding: EdgeInsets.only(top: 20, left: 10, right: 10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[600]),
                borderRadius: BorderRadius.circular(5)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Fecha de vencimiento *',
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black87,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: 5),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: kPrimaryColor, padding: EdgeInsets.zero),
                  onPressed: () {
                    showDatePicker(
                      builder: (BuildContext context, Widget child) {
                        return Theme(
                          data: ThemeData.light().copyWith(
                            colorScheme: ColorScheme.light(
                              primary: kPrimaryColor,
                              onPrimary: Colors.white,
                              surface: kPrimaryColor,
                            ),
                          ),
                          child: child,
                        );
                      },
                      cancelText: 'CANCELAR',
                      helpText: 'SELECCIONE UNA FECHA',
                      fieldLabelText: 'Ingrese una fecha permitida',
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime.now(),
                      lastDate: DateTime(2025),
                    ).then((DateTime value) {
                      if (value != null) {
                        DateTime _fromDate = DateTime.now();
                        _fromDate = value;
                        setState(() {
                          _date = DateFormat.yMMMd().format(_fromDate);
                        });
                      }
                    });
                  },
                  child: SizedBox(
                    width: 150,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.calendar_today_outlined),
                        SizedBox(width: 5),
                        Text('Abrir calendario'),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Text(_date != null
                    ? 'La fecha seleccionada es: $_date'
                    : 'No hay fecha seleccionada'),
                SizedBox(height: 20)
              ],
            ),
          ),
          const SizedBox(height: 24.0),
          /*"Aditional Comments" form.*/
          TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              alignLabelWithHint: true,
              hintText:
                  'Añada comentarios adicionales del producto o del negocio',
              helperText: 'Intente ser lo más descriptivo posible.',
              helperStyle: TextStyle(fontSize: kH5),
              labelText: 'Observaciones',
            ),
            maxLines: 5,
          ),
          const SizedBox(height: 24.0),
          RoundedButton(
            text: "Guardar",
            color: kPrimaryColor,
            width: 150,
            heigth: 50,
            press: () {
              final snackBar = SnackBar(
                backgroundColor: Colors.green,
                behavior: SnackBarBehavior.floating,
                content: Text(
                  '¡La oferta fue actualizada exitosamente!',
                  style: TextStyle(fontSize: kH4),
                ),
              );
              /*Find the Scaffold in the widget tree and use
              it to show a SnackBar.*/
              ScaffoldMessenger.of(context).showSnackBar(snackBar);

              Future.delayed(Duration(seconds: 3), () {
                Navigator.pop(context);
              });
            },
            verticalPadding: 10,
            horizontalPadding: 10,
          ),
          const SizedBox(height: 24.0),
        ],
      ),
    );
  }
}
