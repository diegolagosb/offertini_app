import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Offertini/screens/Login/components/background.dart';
import 'package:Offertini/screens/Signup/signup_screen.dart';
import 'package:Offertini/components/already_have_an_account_acheck.dart';
import 'package:Offertini/components/rounded_button.dart';
import 'package:Offertini/components/rounded_input_field.dart';
import 'package:Offertini/components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';
import 'package:Offertini/screens/home/home_screen.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.05),
            SvgPicture.asset(
              "assets/images/login.svg",
              height: size.height * 0.30,
            ),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Correo electrónico",
              icon: Icons.person,
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              hintText: "Contraseña",
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "INICIAR SESIÓN",
              width: size.width * 0.8,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return HomeScreen();
                    },
                  ),
                );
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
