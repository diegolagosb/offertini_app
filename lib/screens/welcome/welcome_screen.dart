import 'dart:async';

import 'package:Offertini/screens/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:Offertini/screens/welcome/components/body.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  void initState() {
    super.initState();

    Future<void> loadPictures() async {
      await precachePicture(
        ExactAssetPicture(
            SvgPicture.svgStringDecoder, 'assets/images/logo_offertini.svg'),
        context,
      );
      await precachePicture(
        ExactAssetPicture(
            SvgPicture.svgStringDecoder, 'assets/images/welcome_grocery.svg'),
        context,
      );
    }

    loadPictures();

    Timer(
        Duration(seconds: 5),
        () => Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(
        SvgPicture.asset(
          "assets/images/logo_offertini.svg",
          height: 150,
        ),
        SvgPicture.asset(
          "assets/images/welcome_grocery.svg",
          height: 250,
        ),
      ),
    );
  }
}
