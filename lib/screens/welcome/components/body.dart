import 'package:flutter/material.dart';
import 'package:Offertini/screens/welcome/components/background.dart';
import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  Widget logo, img;

  Body(this.logo, this.img);

  @override
  Widget build(BuildContext context) {
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[logo, img],
        ),
      ),
    );
  }
}
