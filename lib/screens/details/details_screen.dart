import 'package:Offertini/screens/details/components/icon_text_btn.dart';
import 'package:Offertini/screens/editoffer/edit_offer_screen.dart';
import 'package:Offertini/screens/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:Offertini/constants.dart';
import 'package:Offertini/models/Product.dart';
import 'package:Offertini/screens/details/components/body.dart';

class DetailsScreen extends StatelessWidget {
  final Product product;

  const DetailsScreen({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // each product have a color
      backgroundColor: product.color,
      appBar: buildAppBar(context),
      body: Body(product: product),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: product.color,
      elevation: 0,
      leading: IconButton(
        icon: SvgPicture.asset(
          'assets/icons/back.svg',
          color: Colors.white,
        ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[
        IconTextBtn(
          press: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return EditOfferScreen();
            }));
          },
          color: Colors.yellow[800],
          text: "Editar",
          textColor: Colors.white,
          icon: Icons.edit,
          iconColor: Colors.white,
        ),
        SizedBox(width: kDefaultPadding / 2),
        IconTextBtn(
          press: () {
            final snackBar = SnackBar(
              backgroundColor: Colors.green,
              behavior: SnackBarBehavior.floating,
              content: Text(
                '¡La oferta fue eliminada exitosamente!',
                style: TextStyle(fontSize: kH4),
              ),
            );
            /*Find the Scaffold in the widget tree and use
              it to show a SnackBar.*/
            ScaffoldMessenger.of(context).showSnackBar(snackBar);

            Future.delayed(Duration(seconds: 2), () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return HomeScreen();
              }));
            });
          },
          color: Colors.red[700],
          text: "Eliminar",
          textColor: Colors.white,
          icon: Icons.delete,
          iconColor: Colors.white,
        ),
        SizedBox(width: kDefaultPadding / 2)
      ],
    );
  }
}
