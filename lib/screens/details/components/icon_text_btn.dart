import 'package:flutter/material.dart';

class IconTextBtn extends StatelessWidget {

  final IconData icon;
  final String text;
  final Color color, textColor, iconColor;
  final Function press;

  const IconTextBtn({
    Key key,
    this.icon,
    this.text,
    this.color,
    this.textColor,
    this.iconColor,
    this.press
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: MaterialButton(
        onPressed: press,
        color: color,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        highlightColor: Colors.black,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: iconColor,
              size: 30,
            ),
            Text(
              text,
              style: TextStyle(color: textColor),
            )
          ],
        ),
      ),
    );
  }
}
