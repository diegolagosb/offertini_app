import 'package:flutter/material.dart';
import 'package:Offertini/models/Product.dart';

import '../../../constants.dart';

class TypeAndConcurrence extends StatelessWidget {
  const TypeAndConcurrence({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Tipo\n"),
                TextSpan(
                  text: product.type,
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Concurrencia\n"),
                if(product.concurrence == 'Alta')
                TextSpan(
                  text: product.concurrence,
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(fontWeight: FontWeight.bold, color: Colors.red),
                )
                else if(product.concurrence == 'Media')
                  TextSpan(
                    text: product.concurrence,
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontWeight: FontWeight.bold, color: Colors.yellow[800]),
                  )
                else
                  TextSpan(
                    text: product.concurrence,
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontWeight: FontWeight.bold, color: Colors.green),
                  )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
