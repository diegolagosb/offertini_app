import 'package:flutter/material.dart';
import 'package:Offertini/models/Product.dart';

import '../../../constants.dart';

class Description extends StatelessWidget {
  const Description({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: kDefaultPadding),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(bottom: 5),
            child: Text(
              "Observaciones",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: kH4),
            ),
          ),
          Text(
            product.description,
            style: TextStyle(height: 1.5),
          ),
        ],
      ),
    );
  }
}
