import 'package:flutter/material.dart';

import '../../../constants.dart';

class LikesCounter extends StatefulWidget {
  @override
  _LikesCounterState createState() => _LikesCounterState();
}

class _LikesCounterState extends State<LikesCounter> {
  int numOfLikes = 0;
  int numOfDislikes = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        buildOutlineButtonDislike(
          icon: Icons.thumb_down,
          pressDislikes: () {
            setState(() {
              numOfDislikes++;
            });
          },
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
          child: Text(
            // if our item is less  then 10 then  it shows 01 02 like that
            numOfDislikes.toString().padLeft(2, "0"),
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        buildOutlineButtonLike(
            icon: Icons.thumb_up,
            pressLikes: () {
              setState(() {
                numOfLikes++;
              });
            }),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
          child: Text(
            // if our item is less  then 10 then  it shows 01 02 like that
            numOfLikes.toString().padLeft(2, "0"),
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
      ],
    );
  }

  SizedBox buildOutlineButtonDislike({IconData icon, Function pressDislikes}) {
    return SizedBox(
      width: 40,
      height: 32,
      child: OutlineButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
        onPressed: pressDislikes,
        child: Icon(icon),
      ),
    );
  }

  SizedBox buildOutlineButtonLike({IconData icon, Function pressLikes}) {
    return SizedBox(
      width: 40,
      height: 32,
      child: OutlineButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
        onPressed: pressLikes,
        child: Icon(icon),
      ),
    );
  }
}
