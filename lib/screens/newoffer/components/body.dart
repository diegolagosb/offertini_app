import 'package:Offertini/constants.dart';
import 'package:Offertini/screens/newoffer/components/form.dart';
import 'package:flutter/material.dart';
import 'package:Offertini/components/background_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Background(
      child: Stack(children: [
        SingleChildScrollView(
          padding: EdgeInsets.only(
              top: kDefaultPadding,
              left: kDefaultPadding * 2,
              right: kDefaultPadding * 2),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[FormNewOffer()],
          ),
        ),
      ]),
    );
  }
}
