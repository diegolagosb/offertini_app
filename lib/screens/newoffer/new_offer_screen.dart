import 'package:Offertini/constants.dart';
import 'package:flutter/material.dart';
import 'package:Offertini/screens/newoffer/components/body.dart';

class NewOfferScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Body(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        elevation: 5,
        title: Text("Nueva Oferta"));
  }
}
