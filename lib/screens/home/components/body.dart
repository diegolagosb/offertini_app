import 'package:Offertini/components/order_by_filters.dart';
import 'package:Offertini/components/productList.dart';
import 'package:flutter/material.dart';
import 'package:Offertini/models/Product.dart';

class Body extends StatelessWidget {
  final String type;

  const Body({Key key, this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    sortList();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 10),
        OrderByFilters(),
        SizedBox(
          height: 20,
        ),
        (type == 'bread')
            ? ProductList()
            : (type == 'meal')
                ? Center(child: Text('Lista de productos de carnes'))
                : Center(child: Text('Lista de productos de bebestibles'))
      ],
    );
  }
}
