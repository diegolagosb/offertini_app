import 'package:Offertini/screens/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../constants.dart';

class DrawerHome extends StatelessWidget {
  const DrawerHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buildDrawer(context);
  }

  Drawer buildDrawer(context) {
    return Drawer(child: drawerItems(context));
  }

  static const drawerHeader = UserAccountsDrawerHeader(
    decoration: BoxDecoration(color: kPrimaryBackgroundColor),
    accountName: Text('User Name'),
    accountEmail: Text('user.name@email.com'),
    currentAccountPicture: CircleAvatar(
      backgroundColor: Colors.white,
      child: Icon(
        Icons.person,
        size: 42.0,
        color: kPrimaryColor,
      ),
    ),
  );

  drawerItems(context) => ListView(
        children: <Widget>[
          drawerHeader,
          ListTile(
            leading: Icon(Icons.looks_one),
            title: Text('Item 1'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.looks_two),
            title: Text('Item 2'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.looks_3),
            title: Text('Item 3'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.info_outline),
            title: Text('Términos y condiciones'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.group_outlined),
            title: Text('¿Quiénes somos?'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.share),
            title: Text('Compartir'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.star),
            title: Text('Calificar'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.feedback),
            title: Text('Sugerencias'),
            onTap: () {},
          ),
          TextButton(
            onPressed: () =>
                Future.delayed(const Duration(milliseconds: 1000), () {
              //TODO: Sign out
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return LoginScreen();
                  },
                ),
              );
            }),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.logout,
                      color: kPrimaryColor,
                      size: 30,
                    ),
                  ),
                  Text(
                    'Cerrar sesión',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: kH3,
                        color: kPrimaryColor),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: kDefaultPadding),
        ],
      );
}
