import 'dart:io';

import 'package:Offertini/screens/home/components/drawer.dart';
import 'package:Offertini/screens/newoffer/new_offer_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:Offertini/constants.dart';
import 'package:Offertini/screens/home/components/body.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: DefaultTabController(
          length: 3,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: buildAppBar(),
            drawer: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                child: SafeArea(child: DrawerHome())),
            body: buildTabBarView(),
            floatingActionButton: FloatingActionButton(
              backgroundColor: kPrimaryColor,
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return NewOfferScreen();
                }));
              },
              child: Icon(Icons.add),
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
        backgroundColor: Color(0xFFF39861),
        elevation: 5,
        leading: FittedBox(
          fit: BoxFit.cover,
          child: CircleAvatar(
              backgroundColor: Colors.transparent,
              maxRadius: 30,
              child: IconButton(
                  onPressed: () {
                    _scaffoldKey.currentState.openDrawer();
                  },
                  icon: Icon(
                    Icons.person,
                    size: 30,
                    color: kPrimaryColor,
                  ))),
        ),
        title: Center(
            child: IconButton(
                iconSize: 200,
                icon: SvgPicture.asset(
                  "assets/images/logo_offertini.svg",
                ))),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.exit_to_app,
              color: kPrimaryColor,
              size: 30,
            ),
            onPressed: () =>
                Future.delayed(const Duration(milliseconds: 1000), () {
              exit(0);
            }),
          ),
          SizedBox(width: kDefaultPadding / 2),
        ],
        bottom: buildTabBar());
  }

  TabBar buildTabBar() {
    return TabBar(
      labelColor: kPrimaryColor,
      labelStyle: TextStyle(fontSize: kH4, fontWeight: FontWeight.bold),
      unselectedLabelStyle: TextStyle(fontSize: kH5),
      unselectedLabelColor: kPrimaryLightColor,
      indicatorColor: kPrimaryColor,
      indicatorWeight: 5,
      tabs: [
        Tab(text: 'PAN'),
        Tab(text: 'CARNE'),
        Tab(text: 'BEBESTIBLE'),
      ],
    );
  }

  TabBarView buildTabBarView() {
    return TabBarView(
      children: [
        Body(
          type: 'bread',
        ),
        Body(
          type: 'meal',
        ),
        Body(
          type: 'drink',
        ),
      ],
    );
  }
}
