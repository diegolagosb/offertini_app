import 'package:Offertini/components/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';

class FormReport extends StatefulWidget {
  const FormReport({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FormReportState();
}

// Adapted from the text form demo in official gallery app:
// https://github.com/flutter/flutter/blob/master/examples/flutter_gallery/lib/demo/material/text_form_field_demo.dart
class _FormReportState extends State<FormReport> {
  String _dropdownValue,
      _articleType,
      _prize,
      _marketName,
      _marketAddress,
      _date;
  double _sliderVal = 50;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          const SizedBox(height: 24.0),
          /*"Aditional Comments" form.*/
          TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              alignLabelWithHint: true,
              hintText:
                  'Añada los motivos principales y específicos del reporte.',
              helperText: 'Intente ser lo más descriptivo posible.',
              helperStyle: TextStyle(fontSize: kH5),
              labelText: 'Comentarios',
            ),
            maxLines: 12,
          ),
          const SizedBox(height: 24.0),
          RoundedButton(
            text: "Enviar",
            color: kPrimaryColor,
            width: 150,
            heigth: 50,
            press: () {
              final snackBar = SnackBar(
                backgroundColor: Colors.green,
                behavior: SnackBarBehavior.floating,
                content: Text(
                  '¡El reporte fue enviado correctamente! Gracias por apoyarnos'
                  ' a mantener una aplicación de calidad para todos.',
                  style: TextStyle(fontSize: kH4),
                ),
              );
              /*Find the Scaffold in the widget tree and use
              it to show a SnackBar.*/
              ScaffoldMessenger.of(context).showSnackBar(snackBar);

              Future.delayed(Duration(seconds: 5), () {
                Navigator.pop(context);
              });
            },
            verticalPadding: 10,
            horizontalPadding: 10,
          ),
          const SizedBox(height: 24.0),
        ],
      ),
    );
  }
}
