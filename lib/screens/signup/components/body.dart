import 'package:Offertini/components/back_btn.dart';
import 'package:Offertini/screens/welcome/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:Offertini/screens/login/login_screen.dart';
import 'package:Offertini/screens/signup/components/background.dart';
import 'package:Offertini/Screens/signup/components/or_divider.dart';
import 'package:Offertini/Screens/signup/components/social_icon.dart';
import 'package:Offertini/components/already_have_an_account_acheck.dart';
import 'package:Offertini/components/rounded_button.dart';
import 'package:Offertini/components/rounded_input_field.dart';
import 'package:Offertini/components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';
import '../../../constants.dart';

class Body extends StatelessWidget {
  void _handleRadioValueChange(bool value) {}

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Stack(children: [
        SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Text(
                "¡Regístrate!",
                style: TextStyle(
                    fontSize: kH1,
                    color: kPrimaryLightColor,
                    fontWeight: FontWeight.bold),
              ),
              SvgPicture.asset(
                "assets/images/people_shopping.svg",
                width: size.width * 0.3,
              ),
              SizedBox(
                width: size.width * 0.8,
                child: Text(
                  "Crear tu cuenta es gratis y sólo te tomará un par de minutos...",
                  style: TextStyle(
                      fontSize: kH4,
                      color: kPrimaryLightColor,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: size.height * 0.03),
              AlreadyHaveAnAccountCheck(
                login: false,
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
              ),
              SizedBox(height: size.height * 0.03),
              RoundedInputField(
                hintText: "Apodo",
                icon: Icons.person,
                onChanged: (value) {},
              ),
              RoundedInputField(
                hintText: "Correo electrónico",
                icon: Icons.email,
                onChanged: (value) {},
              ),
              RoundedPasswordField(
                hintText: "Contraseña",
                onChanged: (value) {},
              ),
              RoundedPasswordField(
                hintText: "Confirma tu contraseña",
                onChanged: (value) {},
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Radio(
                    value: false,
                    onChanged: _handleRadioValueChange,
                  ),
                  SizedBox(
                    child: Text(
                      "Acepto los términos y condiciones",
                      style: TextStyle(
                          fontSize: kH5,
                          color: kPrimaryLightColor,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              RoundedButton(
                text: "REGISTRARME",
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
              ),
              /*SizedBox(height: size.height * 0.03),
              AlreadyHaveAnAccountCheck(
                login: false,
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
              ),
              OrDivider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SocalIcon(
                    iconSrc: "assets/icons/facebook.svg",
                    press: () {},
                  ),
                  SocalIcon(
                    iconSrc: "assets/icons/twitter.svg",
                    press: () {},
                  ),
                  SocalIcon(
                    iconSrc: "assets/icons/google-plus.svg",
                    press: () {},
                  ),
                ],
              )*/
            ],
          ),
        ),
        Positioned(
          child: BackBtn(),
        ),
      ]),
    );
  }
}
