import 'package:Offertini/models/Product.dart';
import 'package:flutter/material.dart';
import '../constants.dart';

class OrderByFilters extends StatelessWidget {
  const OrderByFilters({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          "Ordenar por:",
          style: TextStyle(
              fontSize: kH4,
              fontWeight: FontWeight.w800,
              color: kPrimaryBackgroundColor),
        ),
        OutlinedButton.icon(
            style: OutlinedButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 5),
                side: BorderSide(color: kPrimaryColor)),
            onPressed: () {
              sortList;
            },
            icon: Icon(
              Icons.attach_money,
              size: 18,
              color: kPrimaryColor,
            ),
            label: Text(
              "PRECIO",
              style: TextStyle(color: kPrimaryColor),
            )),
        OutlinedButton.icon(
          style: OutlinedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 5),
              side: BorderSide(color: kPrimaryColor)),
          onPressed: () {
            // Respond to button press
          },
          icon: Icon(
            Icons.favorite,
            size: 18,
            color: kPrimaryColor,
          ),
          label: Text(
            "VALORACIÓN",
            style: TextStyle(color: kPrimaryColor),
          ),
        ),
      ],
    );
  }
}
