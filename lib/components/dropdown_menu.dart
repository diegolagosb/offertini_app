import 'package:Offertini/constants.dart';
import 'package:flutter/material.dart';

class DropdownMenu extends StatefulWidget {
  @override
  _DropdownMenuState createState() =>
      _DropdownMenuState();
}

class _DropdownMenuState extends State<DropdownMenu> {
  String dropdownValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
          isExpanded: true,
          value: dropdownValue,
          hint: Row(
            children: [
              Container(
                child: Icon(Icons.article, color: Colors.grey,),
              ),
              SizedBox(width: kDefaultPadding + 5,),
              Container(
                child: Text("Seleccione un artículo *"),
              )
            ],
          ),
          items: [
            DropdownMenuItem<String> (
              value: '1',
              child: Text('Pan'),
            ),
            DropdownMenuItem<String> (
              value: '2',
              child: Text('Carne'),
            ),
            DropdownMenuItem<String> (
              value: '3',
              child: Text('Bebestible'),
            ),
          ],
          onChanged: (value) {
            setState(() {
              dropdownValue = value;
            });
          },
    );
  }
}