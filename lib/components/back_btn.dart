import 'package:Offertini/screens/welcome/welcome_screen.dart';
import 'package:flutter/material.dart';
import '../constants.dart';

class BackBtn extends StatelessWidget {
  const BackBtn({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: IconButton(
        icon: Icon(Icons.arrow_back),
        iconSize: 35,
        color: kPrimaryColor,
        onPressed: () => Navigator.pop(context)
      ),
    );
  }
}
