import 'package:Offertini/models/Product.dart';
import 'package:Offertini/screens/details/details_screen.dart';
import 'package:Offertini/screens/home/components/item_card.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class ProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
        child: GridView.builder(
            itemCount: products.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: kDefaultPadding,
              crossAxisSpacing: kDefaultPadding,
              childAspectRatio: 0.75,
            ),
            padding: EdgeInsets.only(bottom: kDefaultPadding * 4),
            itemBuilder: (context, index) => ItemCard(
              product: products[index],
              press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailsScreen(
                      product: products[index],
                    ),
                  )),
            )),
      ),
    );
  }
}
