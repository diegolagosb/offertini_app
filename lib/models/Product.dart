import 'package:flutter/material.dart';

class Product {
  final String image,
      title,
      description,
      addedAt,
      expirationDate,
      type,
      concurrence;
  final int price, size, id;
  final Color color;
  Product(
      {this.id,
      this.image,
      this.title,
      this.price,
      this.description,
      this.size,
      this.color,
      this.addedAt,
      this.expirationDate,
      this.type,
      this.concurrence});
}

List<Product> products = [
  Product(
      id: 1,
      title: "Minimarket El pato",
      price: 800,
      type: "Marraqueta",
      description: dummyText,
      image: "assets/images/bread.png",
      color: Color(0xFF3D82AE),
      addedAt: "2021-04-12",
      expirationDate: "2021-04-16",
      concurrence: "Baja"),
  Product(
      id: 2,
      title: "Unimarc",
      type: "Marraqueta",
      price: 900,
      description: dummyText,
      image: "assets/images/bread.png",
      color: Color(0xFFD3A984),
      addedAt: "2021-04-12",
      expirationDate: "2021-04-16",
      concurrence: "Alta"),
  Product(
      id: 3,
      title: "Minimarket Doña Ely",
      type: "Marraqueta",
      price: 650,
      description: dummyText,
      image: "assets/images/bread.png",
      color: Color(0xFF989493),
      addedAt: "2021-04-12",
      expirationDate: "2021-04-16",
      concurrence: "Alta"),
  Product(
      id: 4,
      title: "El trébol",
      type: "Marraqueta",
      price: 750,
      description: dummyText,
      image: "assets/images/bread.png",
      color: Color(0xFFE6B398),
      addedAt: "2021-04-12",
      expirationDate: "2021-04-16",
      concurrence: "Baja"),
  Product(
      id: 5,
      title: "Ricopan",
      type: "Marraqueta",
      price: 890,
      description: dummyText,
      image: "assets/images/bread.png",
      color: Color(0xFFFB7883),
      addedAt: "2021-04-12",
      expirationDate: "2021-04-16",
      concurrence: "Alta"),
  Product(
      id: 6,
      title: "Panadería Las encinas",
      type: "Marraqueta",
      price: 900,
      description: dummyText,
      image: "assets/images/bread.png",
      color: Color(0xFFAEAEAE),
      addedAt: "2021-04-12",
      expirationDate: "2021-04-16",
      concurrence: "Media"),
];

void sortList() => {products.sort((a, b) => a.price.compareTo(b.price))};

String dummyText =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. When an unknown printer took a galley.";
