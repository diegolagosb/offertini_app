import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFE05B28);
const kPrimaryLightColor = Color(0xFFF1E6FF);
const kPrimaryBackgroundColor = Color(0xFFF39861);
const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);

const kDefaultPadding = 20.0;

const kH1 = 32.0;
const kH2 = 24.0;
const kH3 = 20.0;
const kH4 = 16.0;
const kH5 = 12.0;
